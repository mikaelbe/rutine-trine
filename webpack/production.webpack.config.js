/* eslint no-var: 0 */
var path = require('path');
var webpack = require('webpack');

module.exports = {
  minimize: true,
  entry: [
    './src/client.js',
  ],
  output: {
    path: path.join(__dirname, '../dist'),
    publicPath: '/static/',
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['', '.js'],
  },
  plugins: [
    new webpack.DefinePlugin({
      __DEVTOOLS__: false,
    }),
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader'],
      },
    ],
  },
};
