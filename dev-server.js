/* eslint no-var:0 no-console:0 */
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack/development.webpack.config');

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  stats: {
    colors: true,
  },
}).listen(3000, 'localhost', function inner(err) {
  if (err) {
    console.log(err);
  }

  console.log('Dev server listening at localhost:3000');
});
