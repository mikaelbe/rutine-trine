import assign from 'lodash.assign';
import {normalize, Schema, arrayOf} from 'normalizr';
import api from '../api';

const routineSchema = new Schema('routines');

const runSchema = new Schema('runs');

export const Schemas = {
  ROUTINE: routineSchema,
  ROUTINE_ARRAY: arrayOf(routineSchema),
  RUN: runSchema,
  RUN_ARRAY: arrayOf(runSchema),
};

export const Requests = {
  GET: 'GET',
  LIST: 'LIST',
  CREATE: 'CREATE',
  UPDATE: 'UPDATE',
  DELETE: 'DELETE',
};

export const CALL_API = Symbol('Call API');

function callApi(request, schema, id = null, payload = {}) {
  return api(request, schema, id, payload)
    .then(response => {
      return assign({}, normalize(response, schema));
    });
}

/**
 * Redux middleware which interprets actions with CALL_API info specified.
 * Performs the call and promises when such actions are dispatched. Should be
 * given `types` (array of 3 action types for "request", "success" and
 * "failure"), `schema` (see above), `request` (see above) and `id`/`payload`
 * as fitting.
 */
export default store => next => action => {
  const callAPI = action[CALL_API];
  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  const {schema, types, bailout, request, payload, id} = callAPI;

  if (!schema) {
    throw new Error('Specify one of the exported Schemas.');
  }
  if (!Array.isArray(types) || types.length !== 3) {
    throw new Error('Expected an array of three action types.');
  }
  if (!types.every(type => typeof type === 'string')) {
    throw new Error('Expected actions types to be strings.');
  }
  if (typeof bailout !== 'undefined' && typeof bailout !== 'function') {
    throw new Error('Expected bailout to be either undefined or a function');
  }

  if (!Requests[request]) {
    throw new Error(`Expected request to be one of the exported request types, got ${request}`);
  }

  // All requests other than LIST and CREATE need an ID
  if (request !== Requests.LIST && request !== Requests.CREATE && !id) {
    throw new Error(`Need an id specified to perform ${request}`);
  }

  if (bailout && bailout(store.getState())) {
    return Promise.resolve();
  }

  function actionWith(data) {
    const finalAction = assign({}, action, data);
    delete finalAction[CALL_API];
    return finalAction;
  }

  const [requestType, successType, failureType] = types;
  next(actionWith({type: requestType}));

  return callApi(request, schema, id, payload).then(
    response => next(actionWith({
      response,
      type: successType,
    })),
    error => next(actionWith({
      type: failureType,
      error: error.message || 'Something bad happened',
    }))
  );
};
