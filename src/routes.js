import React from 'react';
import {Route} from 'react-router';
import App from './containers/App';
import RoutinePage from './containers/RoutinePage';

export default [
  <Route path="/" component={App}>
    <Route path="routines/:routineId" component={RoutinePage} />
  </Route>,
];
