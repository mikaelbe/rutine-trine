import {CALL_API, Schemas, Requests} from '../middleware/apiMiddleware';

export const LIST_ROUTINES_REQUEST = 'LIST_ROUTINES_REQUEST';
export const LIST_ROUTINES_SUCCESS = 'LIST_ROUTINES_SUCCESS';
export const LIST_ROUTINES_FAILURE = 'LIST_ROUTINES_FAILURE';

export function listRoutines() {
  return {
    [CALL_API]: {
      types: [LIST_ROUTINES_REQUEST, LIST_ROUTINES_SUCCESS, LIST_ROUTINES_FAILURE],
      schema: Schemas.ROUTINE_ARRAY,
      request: Requests.LIST,
    },
  };
}

export const LIST_RUNS_REQUEST = 'LIST_RUNS_REQUEST';
export const LIST_RUNS_SUCCESS = 'LIST_RUNS_SUCCESS';
export const LIST_RUNS_FAILURE = 'LIST_RUNS_FAILURE';

export function listRuns() {
  return {
    [CALL_API]: {
      types: [LIST_RUNS_REQUEST, LIST_RUNS_SUCCESS, LIST_RUNS_FAILURE],
      schema: Schemas.RUN_ARRAY,
      request: Requests.LIST,
    },
  };
}

export const LOAD_ROUTINE_REQUEST = 'LOAD_ROUTINE_REQUEST';
export const LOAD_ROUTINE_SUCCESS = 'LOAD_ROUTINE_SUCCESS';
export const LOAD_ROUTINE_FAILURE = 'LOAD_ROUTINE_FAILURE';

export function loadRoutine(routineId) {
  return {
    [CALL_API]: {
      types: [LOAD_ROUTINE_REQUEST, LOAD_ROUTINE_SUCCESS, LOAD_ROUTINE_FAILURE],
      schema: Schemas.ROUTINE,
      request: Requests.GET,
      id: routineId,
    },
  };
}

export const ADD_ROUTINE_REQUEST = 'ADD_ROUTINE_REQUEST';
export const ADD_ROUTINE_SUCCESS = 'ADD_ROUTINE_SUCCESS';
export const ADD_ROUTINE_FAILURE = 'ADD_ROUTINE_FAILURE';

export function addRoutine(routine) {
  return {
    [CALL_API]: {
      types: [ADD_ROUTINE_REQUEST, ADD_ROUTINE_SUCCESS, ADD_ROUTINE_FAILURE],
      schema: Schemas.ROUTINE,
      request: Requests.CREATE,
      payload: routine,
    },
  };
}

export const EDIT_ROUTINE_REQUEST = 'EDIT_ROUTINE_REQUEST';
export const EDIT_ROUTINE_SUCCESS = 'EDIT_ROUTINE_SUCCESS';
export const EDIT_ROUTINE_FAILURE = 'EDIT_ROUTINE_FAILURE';

export function editRoutine(routineId, routine) {
  return {
    [CALL_API]: {
      types: [EDIT_ROUTINE_REQUEST, EDIT_ROUTINE_SUCCESS, EDIT_ROUTINE_FAILURE],
      schema: Schemas.ROUTINE,
      request: Requests.UPDATE,
      id: routineId,
      payload: routine,
    },
  };
}

export const EDIT_RUN_REQUEST = 'EDIT_RUN_REQUEST';
export const EDIT_RUN_SUCCESS = 'EDIT_RUN_SUCCESS';
export const EDIT_RUN_FAILURE = 'EDIT_RUN_FAILURE';

export function editRun(runId, run) {
  return {
    [CALL_API]: {
      types: [EDIT_RUN_REQUEST, EDIT_RUN_SUCCESS, EDIT_RUN_FAILURE],
      schema: Schemas.RUN,
      request: Requests.UPDATE,
      id: runId,
      payload: run,
    },
  };
}

export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE';
