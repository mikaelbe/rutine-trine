import merge from 'lodash.merge';
import * as ActionTypes from '../actions';

/**
 * Update cache with entities from any action's `.response.entities`.
 */
export function entities(state = {routines: {}, runs: {}}, action) {
  if (action.response && action.response.entities) {
    return merge({}, state, action.response.entities, (a, b) => {
      if (Array.isArray(a)) {
        return b;
      }
    });
  }

  return state;
}

/**
 * Picks `.error` from any action. Resets on RESET_ERROR_MESSAGE.
 */
export function errorMessage(state = null, action) {
  const {type, error} = action;

  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null;
  } else if (error) {
    return error;
  }

  return state;
}
