import {expect} from 'chai';
import {entities, errorMessage} from '../index';
import * as ActionTypes from '../../actions';

const response = {
  result: [1, 2],
  entities: {
    routines: {
      1: {
        name: 'New Employee',
      },
      2: {
        name: 'Fire Employee',
      },
    },
  },
};

describe('entities reducer', () => {
  it('builds `routines` state', () => {
    expect(entities(undefined, {}).routines).to.be.an('object');
  });

  it('builds `runs` cache', () => {
    expect(entities(undefined, {}).runs).to.be.an('object');
  });

  it('does not mutate', () => {
    const state = {};
    expect(entities(state, {response})).not.to.equal(state);
  });

  it('adds routines from action', () => {
    const state = entities(undefined, {response});
    expect(state.routines).to.deep.equal(response.entities.routines);
  });
});

describe('errorMessage reducer', () => {
  it('builds `errorMessage` state', () => {
    expect(errorMessage(undefined, {})).to.equal(null);
  });

  it('passes through if no error', () => {
    const state = {};
    expect(errorMessage(state, {})).to.deep.equal(state);
  });

  it('stores any action\'s `.error`', () => {
    const error = new Error('oops');
    expect(errorMessage({}, {error})).to.equal(error);
  });

  it('resets on `RESET_ERROR_MESSAGE`', () => {
    expect(
      errorMessage({error: 1}, {type: ActionTypes.RESET_ERROR_MESSAGE})
    ).to.equal(null);
  });
});
