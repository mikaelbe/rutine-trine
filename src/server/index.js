import path from 'path';
import express from 'express';
import createApiRouter from './api';
import React from 'react';
import Router from 'react-router';
import Location from 'react-router/lib/Location';
import routes from '../routes';
import configureStore from '../store/configureStore';
import getInitialState from '../store/getInitialState';
import {Provider} from 'react-redux';
import Html from '../containers/Html';

const app = express();
export default app;

app.use('/api', createApiRouter('db.json'));

app.use('/static', express.static('dist'));

app.use('/', (req, res) => {
  const location = new Location(req.path, req.query);
  const data = require(path.join(process.cwd(), './db.json'));
  const store = configureStore(getInitialState(data));

  Router.run(routes, location, (err, initialState) => {
    if (!initialState) {
      return res.status(404).send('404 not found');
    }

    const contents = React.renderToString(
      <div>
        <Provider store={store}>
          {() =>
            <Router {...initialState}>
              {routes}
            </Router>
          }
        </Provider>
      </div>
    );

    const html = React.renderToStaticMarkup(<Html contents={contents} data={data} />);
    res.send(html);
  });
});
