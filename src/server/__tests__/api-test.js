import supertest from 'supertest';
import jsonServer from 'json-server';
import createApiRouter from '../api';

const server = jsonServer.create();
const router = createApiRouter(`${__dirname}/dbfixtures.json`);

server.use(router);

describe('api', () => {
  describe('/routines', () => {
    it('should return JSON', (cb) => {
      supertest(server).get('/routines')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, cb);
    });
  });
});
