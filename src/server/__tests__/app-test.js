import supertest from 'supertest';
import app from '../index';

describe('app', () => {
  describe('/', () => {
    it('returns 200', (cb) => {
      supertest(app).get('/').expect(200).end(cb);
    });
  });

  describe('/api', () => {
    it('has working API', (cb) => {
      supertest(app).get('/api')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(404, cb);
    });
  });
});
