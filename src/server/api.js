import jsonServer from 'json-server';

export default function createApiRouter(dbfile) {
  return jsonServer.router(dbfile);
}
