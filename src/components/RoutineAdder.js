import React from 'react';

export default class RoutineAdder extends React.Component {
  constructor() {
    super();
    this.state = {opened: false};

    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  renderClosed() {
    return <button onClick={this.open}>Add Routine</button>;
  }

  renderOpened() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" placeholder="Routine name ..." ref="name" />
        <button type="submit">Add</button>
        <button onClick={this.close}>Cancel</button>
      </form>
    );
  }

  render() {
    const {opened} = this.state;

    return (
      <div className="RoutineAdder">
        {() => {
          return opened ? this.renderOpened() : this.renderClosed();
        }()}
      </div>
    );
  }

  handleSubmit(e) {
    const name = React.findDOMNode(this.refs.name).value.trim();

    e.preventDefault();

    if (this.props.onAdd) {
      this.props.onAdd({name});
    }

    this.close();
  }

  open() {
    this.setState({opened: true});
  }

  close(e) {
    if (e) {
      e.preventDefault();
    }
    this.setState({opened: false});
  }
}

RoutineAdder.propTypes = {
  onAdd: React.PropTypes.func.isRequired,
};
