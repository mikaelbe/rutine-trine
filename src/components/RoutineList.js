import React from 'react';
import RoutineListItem from './RoutineListItem';

export default class RoutineList extends React.Component {
  render() {
    const {routines} = this.props;

    return (
      <ul>
        {() => {
          return routines.map(
            (routine) => <li><RoutineListItem routine={routine} key={routine.id} /></li>
          );
        }()}
      </ul>
    );
  }
}

RoutineList.propTypes = {
  routines: React.PropTypes.array.isRequired,
};
