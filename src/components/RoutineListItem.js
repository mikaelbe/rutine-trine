import React from 'react';
import {Link} from 'react-router';

export default class RoutineListItem extends React.Component {
  render() {
    const {routine} = this.props;

    return (
      <div>
        <Link to={`/routines/${routine.id}`}>
          {routine.name}
        </Link>
        <br />
        <small>({(routine.tasks || []).length} tasks)</small>
      </div>
    );
  }
}

RoutineListItem.propTypes = {
  routine: React.PropTypes.object.isRequired,
};
