import assign from 'lodash.assign';
import indexBy from 'lodash.indexBy';
import map from 'lodash.map';

const cache = {
  routines: {},
  runs: {},
};

// Seed cache with data from server
if (process.browser && window.__DATA__) {
  const {routines, runs} = window.__DATA__;
  cache.routines = assign({}, indexBy(routines, 'id'));
  cache.runs = assign({}, indexBy(runs, 'id'));
}

function createRoutine(_, data) {
  return new Promise((resolve, reject) => {
    if (!data.name) {
      return reject(new Error('Routine needs a name'));
    }

    const id = Math.floor(Math.random() * 1e6);
    const routine = assign({}, data, {id});

    cache.routines[id] = routine;
    return resolve(routine);
  });
}

function getRoutine(id) {
  return new Promise((resolve, reject) => {
    if (!id) {
      return reject(new Error('Need an id'));
    }

    if (cache.routines[id]) {
      resolve(assign({}, cache.routines[id]));
    }

    reject(new Error(`Couldn't find routine ${id}`));
  });
}

function createUpdater(entityName) {
  return function update(id, edits) {
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error(`Need id to edit ${entityName}`));
      }

      if (cache[entityName][id]) {
        const entity = assign({}, cache[entityName][id], edits);
        cache[entityName][id] = entity;
        return resolve(entity);
      }

      reject(new Error(`Couldn't find ${entityName} ${id}`));
    });
  };
}

function createLister(entityName) {
  return function list() {
    return new Promise(resolve => {
      resolve(map(cache[entityName], entity => entity));
    });
  };
}

const functions = {
  CREATE: {
    routines: createRoutine,
  },
  GET: {
    routines: getRoutine,
  },
  UPDATE: {
    routines: createUpdater('routines'),
    runs: createUpdater('runs'),
  },
  LIST: {
    routines: createLister('routines'),
    runs: createLister('runs'),
  },
};

export default function api(request, schema, id, payload) {
  const key = schema._key || schema._itemSchema._key;

  if (functions[request] && functions[request][key]) {
    return functions[request][key](id, payload);
  }

  return new Promise((resolve, reject) => {
    reject(new Error(`Not yet implemented: ${request} ${key}`));
  });
}
