import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import ExplorePage from './ExplorePage';

class App extends React.Component {
  renderChildren() {
    const {children} = this.props;

    if (children) {
      return children;
    }

    return <ExplorePage />;
  }

  render() {
    return (
      <div>
        <h1>Rutine-Trine</h1>
        <Link to="/">Routine Overview</Link>

        {this.renderChildren()}
      </div>
    );
  }
}

App.propTypes = {
  errorMessage: PropTypes.string,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }),
  children: PropTypes.object,
};

App.contextTypes = {
  router: PropTypes.object.isRequired,
};

// Selects the parts of the redux state to give to app as props
function select(state) {
  return {
    errorMessage: state.errorMessage,
  };
}

export default connect(select)(App);
