import React from 'react';
import {connect} from 'react-redux';
import map from 'lodash.map';
import RoutineList from '../components/RoutineList';
import RoutineAdder from '../components/RoutineAdder';
import {addRoutine} from '../actions';

class ExplorePage extends React.Component {
  constructor() {
    super();
    this.onAddRoutine = this.onAddRoutine.bind(this);
  }

  onAddRoutine(routine) {
    this.props.addRoutine(routine);
  }

  render() {
    return (
      <div>
        <RoutineList routines={this.props.routines} />
        <RoutineAdder onAdd={this.onAddRoutine} />
      </div>
    );
  }
}

ExplorePage.propTypes = {
  routines: React.PropTypes.array.isRequired,
  addRoutine: React.PropTypes.func.isRequired,
};

function denormalize(entity) {
  return map(entity, (item) => {
    return item;
  });
}

function select(state) {
  const {entities: {routines, runs}} = state;

  return {
    routines: denormalize(routines),
    runs,
  };
}

export default connect(
  select,
  { addRoutine }
)(ExplorePage);
