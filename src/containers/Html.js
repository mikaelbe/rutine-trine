import React from 'react';

export default class Layout extends React.Component {
  render() {
    return (
      <html>
        <head>
          <title>Rutine-Trine</title>
        </head>
        <body>
          <div dangerouslySetInnerHTML={{__html: this.props.contents}}
              id="root" />

            {() => {
              if (this.props.data) {
                const js = `window.__DATA__ = ${JSON.stringify(this.props.data)};`;
                return <script dangerouslySetInnerHTML={{__html: js}}></script>;
              }
            }()}
          <script src="/static/bundle.js"></script>
        </body>
      </html>
    );
  }
}

Layout.propTypes = {
  contents: React.PropTypes.string.isRequired,
  data: React.PropTypes.object,
};
