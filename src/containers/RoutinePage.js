import React from 'react';
import {connect} from 'react-redux';
import filter from 'lodash.filter';
import indexBy from 'lodash.indexBy';
import assign from 'lodash.assign';
import {loadRoutine, editRoutine, editRun} from '../actions';
import humanizeDuration from 'humanize-duration';
import TaskAdder from '../components/TaskAdder';

function loadData(props) {
  const {routineId} = props;
  props.loadRoutine(routineId);
}

class RoutinePage extends React.Component {
  constructor() {
    super();

    this.onAddTask = this.onAddTask.bind(this);
    this.onDeleteTask = this.onDeleteTask.bind(this);
    this.renderRun = this.renderRun.bind(this);
    this.onMarkRunTaskComplete = this.onMarkRunTaskComplete.bind(this);
  }

  componentWillMount() {
    loadData(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.routineId !== this.props.routineId) {
      loadData(nextProps);
    }
  }

  onAddTask(task) {
    const {routine} = this.props;
    const tasks = routine.tasks || [];
    this.props.editRoutine(routine.id, {tasks: tasks.concat([task])});
  }

  onDeleteTask(taskIndex) {
    const {routine} = this.props;

    const head = routine.tasks.slice(0, taskIndex);
    const tail = routine.tasks.slice(taskIndex + 1);

    this.props.editRoutine(routine.id, {tasks: [].concat(head, tail)});
  }

  onMarkRunTaskComplete(runId, taskId) {
    const {runsForRoutine} = this.props;
    const run = indexBy(runsForRoutine, 'id')[runId];

    const editedTasks = run.tasks.map(task => {
      if (task.taskId === taskId) {
        return assign({}, task, {completed: true});
      }

      return task;
    });

    this.props.editRun(runId, {tasks: editedTasks});
  }

  renderMaxDuration() {
    const {maxDuration} = this.props.routine;
    return maxDuration ? humanizeDuration(maxDuration * 1000) : 'Not set';
  }

  renderTasks() {
    const tasks = this.props.routine.tasks || [];
    return tasks.map((task, index) =>
      <li key={task.name}>
        {task.name}
        <button onClick={() => this.onDeleteTask(index)}>Delete</button>
      </li>
    );
  }

  renderRun(run) {
    const routineTasks = this.props.routine.tasks || [];
    const routineTasksById = indexBy(routineTasks || [], 'id');

    return (
      <li>
        <strong>{run.name}</strong>
        <ol>
          {run.tasks.map(task => {
            /* eslint consistent-return:0 */
            const routineTask = routineTasksById[task.taskId];

            if (!routineTask) {
              return;
            }

            if (task.completed) {
              return (
                <li>
                  <strike>{routineTask.name}</strike>
                </li>
              );
            }

            return (
              <li>
                {routineTask.name}
                <button onClick={() => this.onMarkRunTaskComplete(run.id, task.taskId)}>Done</button>
              </li>
            );
          })}
        </ol>
      </li>
    );
  }

  render() {
    const {routine, runsForRoutine} = this.props;

    if (!routine) {
      return <h2>Loading ...</h2>;
    }

    return (
      <div className="RoutinePage">
        <h2>{routine.name}</h2>

        <dl>
          <dt>Max Duration</dt>
          <dd>{this.renderMaxDuration()}</dd>
        </dl>

        <h3>Tasks</h3>
        <ul>
          {this.renderTasks()}
          <TaskAdder onAdd={this.onAddTask} />
        </ul>

        <h3>Runs</h3>
        <ul>
          {runsForRoutine.map(this.renderRun)}
        </ul>
      </div>
    );
  }
}

RoutinePage.propTypes = {
  routineId: React.PropTypes.string.isRequired,
  routine: React.PropTypes.object,
  loadRoutine: React.PropTypes.func.isRequired,
  editRoutine: React.PropTypes.func.isRequired,
  editRun: React.PropTypes.func.isRequired,
  runsForRoutine: React.PropTypes.array,
};

function select(state, ownProps) {
  const {routineId} = ownProps.params;
  const {entities: {routines, runs}} = state;


  const runsForRoutine = filter(runs, (run) => {
    return run.routineId.toString() === routineId;
  });

  return {
    routineId,
    routine: routines[routineId],
    runsForRoutine,
  };
}

export default connect(
  select,
  {loadRoutine, editRoutine, editRun}
)(RoutinePage);
