import React from 'react';
import {Provider} from 'react-redux';
import {Router} from 'react-router';
import configureStore from '../store/configureStore';
import {listRoutines, listRuns} from '../actions';
import routes from '../routes';
import getInitialState from '../store/getInitialState';

/* eslint one-var:0 */
let DevTools, DebugPanel, LogMonitor;

if (__DEVTOOLS__) {
  DevTools = require('redux-devtools/lib/react').DevTools;
  DebugPanel = require('redux-devtools/lib/react').DebugPanel;
  LogMonitor = require('redux-devtools/lib/react').LogMonitor;
}

const store = configureStore(getInitialState());

export default class Root extends React.Component {
  componentDidMount() {
    store.dispatch(listRoutines());
    store.dispatch(listRuns());
  }

  render() {
    return (
      <div>
        <Provider store={store}>
          {() =>
            <Router history={this.props.history}>
              {routes}
            </Router>
          }
        </Provider>
        {() => {
          if (__DEVTOOLS__) {
            return (
              <DebugPanel top right bottom>
                <DevTools store={store}
                          monitor={LogMonitor} />
              </DebugPanel>
            );
          }
        }()}
      </div>
    );
  }
}

Root.propTypes = {
  history: React.PropTypes.object.isRequired,
};
