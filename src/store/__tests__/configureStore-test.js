import {expect} from 'chai';
import configureStore from '../configureStore';
import * as reducers from '../../reducers';

describe('configureStore', () => {
  it('returns store given no initial state', () => {
    expect(configureStore()).to.be.an('object');
  });

  it('returns store given initial state', () => {
    const initialState = {coffe: true};
    expect(configureStore(initialState)).to.be.an('object');
  });

  it('builds default state with all reducers', () => {
    const defaultState = configureStore().getState();
    expect(Object.keys(defaultState).every(key => !!reducers[key])).to.equal(true);
  });
});
