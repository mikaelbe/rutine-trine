/*
This will try to get entities from `window.__DATA__` and format it as an initial
state for the store to use.
 */
import {normalize} from 'normalizr';
import merge from 'lodash.merge';
import {Schemas} from '../middleware/apiMiddleware';

export default function getInitialState(givenData) {
  let data;

  if (process.browser && window.__DATA__ && !givenData) {
    data = window.__DATA__;
  } else {
    data = givenData;
  }

  if (!data) {
    return {};
  }

  const state = merge({},
    normalize(data.routines, Schemas.ROUTINE_ARRAY),
    normalize(data.runs, Schemas.RUN_ARRAY)
  );

  delete state.result;

  return state;
}
