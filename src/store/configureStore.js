import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import createLogger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import {devTools, persistState} from 'redux-devtools';
import apiMiddleware from '../middleware/apiMiddleware';
import * as reducers from '../reducers';

const loggerMiddleware = createLogger({
  level: 'info',
  collapsed: true,
});

const reducer = combineReducers(reducers);

let withMiddleware;
if (process.browser) {
  withMiddleware = applyMiddleware(
    thunkMiddleware,
    apiMiddleware,
    loggerMiddleware
  );
} else {
  withMiddleware = applyMiddleware(
    thunkMiddleware,
    apiMiddleware
  );
}


let finalCreateStore;

if (process.browser && __DEVTOOLS__) {
  finalCreateStore = compose(
    withMiddleware,
    devTools(),
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/)),
    createStore
  );
} else {
  finalCreateStore = withMiddleware(createStore);
}


export default function configureStore(initialState) {
  return finalCreateStore(reducer, initialState);
}
