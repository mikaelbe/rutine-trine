import app from './src/server';

const server = app.listen(3000, () => {
  /* eslint no-console:0 */
  const {address, port} = server.address();
  console.log(`App listening at http://${address}:${port}`);
});
