# Rutine-Trine

## Todo

 - [ ] Ability to start rutine run
 - [x] Clean up server/client
 - [x] Clean up api middleware
 - [x] Clean up container/component Routine/naming
 - [x] Clean up separation between Routine page/component
 - [x] Generate fake data
 - [x] Incorporate runs
 - [x] Isomorphic rendering
 - [x] Better way of server-data reuse (`initialData` to store)
 - [ ] Demonstrate server API
 - [ ] Better `README.md`
 - [x] Clean up API interface
 - [ ] Light styling
 - [x] Fix broken tests
 - [x] Fix lint errors
