/*
 * Generates some random data to play with
 *
 * Usage
 *
 *     dev-generate-data [outfile]
 *
 * Example
 *
 * 		 dev-generate-data
 *     dev-generate-data db.json
 */
/* eslint no-var:0 vars-on-top:0 */
var fs = require('fs');
var faker = require('faker');
var indexBy = require('lodash.indexBy');

var ROUTINES = 10;
var TASKS_PER_ROUTINE = 10;
var RUNS_PER_ROUTINE = 5;

var outFilename = process.argv.length > 2 ? process.argv[2] : null;

function fillArray(n, fn) {
  return (Array.apply(null, Array(n))).map(fn);
}

var routines = fillArray(ROUTINES, createFakeRoutine);
var routineIds = routines.map(function(routine) {
  return routine.id;
});
var runs = fillArray(ROUTINES * RUNS_PER_ROUTINE, createFakeRun);

var collection = {
  routines: routines,
  runs: runs,
};

if (outFilename) {
  fs.writeFileSync(outFilename, JSON.stringify(collection, null, 2));
} else {
  /* eslint no-console: 0 */
  console.log(collection);
}

function createFakeTask() {
  var name = faker.fake('{{company.bsBuzz}} {{random.number}}');
  return {
    name: name,
    id: faker.helpers.slugify(name),
  };
}

function createFakeRoutine() {
  return {
    id: Math.floor(Math.random() * 1e6),
    name: faker.fake('{{hacker.verb}} {{hacker.noun}}'),
    maxDuration: Math.floor(Math.random() * 30 + 1) * 86400,
    tasks: fillArray(
      Math.floor(Math.random() * TASKS_PER_ROUTINE),
      createFakeTask
    ),
  };
}

function createFakeRun() {
  var routineId = routineIds[Math.floor(Math.random() * routineIds.length)];
  var routine = indexBy(routines, 'id')[routineId];

  var tasks = fillArray(routine.tasks.length, function(_, index) {
    var len = routine.tasks.length;
    return {
      completed: Math.round(Math.random() * 2 * (len - index) / len) === 1, // more likely for small indexes
      taskId: routine.tasks[index].id,
    };
  });

  return {
    id: Math.floor(Math.random() * 1e6),
    routineId: routineId,
    name: faker.fake('{{company.companyName}} {{random.number}}'),
    tasks: tasks,
  };
}
